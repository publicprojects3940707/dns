<?php

namespace App\Console\Commands;

use App\Models\Host;
use App\Services\HostService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class UpdateDnsRecords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dns:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновляет все dns записи всех хостов';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        /** @var HostService $hostService */
        $hostService = app(HostService::class);

        /** @var Host $host */
        foreach (Host::query()->cursor() as $host) {
            try {
                $hostService->syncDnsRecordsFromHost($host);
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
            }
        }
    }
}
