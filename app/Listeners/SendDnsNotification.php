<?php

namespace App\Listeners;

use App\Events\DnsReceived;
use App\Models\User;
use App\Notifications\FirstEntry;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class SendDnsNotification implements ShouldQueue
{
    private User $user;

    /**
     * Create the event listener.
     */
    public function __construct()
    {
        $this->user = Auth::user();
    }

    /**
     * Handle the event.
     */
    public function handle(DnsReceived $event): void
    {
        $this->user->notify(new FirstEntry());
        $this->setParamsToUserSession();
    }

    /**
     * Determine whether the listener should be queued.
     */
    public function shouldQueue(DnsReceived $event): bool
    {
        return empty(session('dnsWatched')) || !session('dnsWatched');
    }

    private function setParamsToUserSession(): void
    {
        session(['dnsWatched' => true]);
    }
}
