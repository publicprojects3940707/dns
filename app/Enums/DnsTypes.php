<?php

namespace App\Enums;

enum DnsTypes: string
{
    case A = 'A';
    case AAAA = 'AAAA';
    case PTR = 'PTR';

    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }
}
