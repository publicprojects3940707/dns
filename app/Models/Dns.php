<?php

namespace App\Models;

use App\Casts\Json;
use App\Enums\DnsTypes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Log;

/**
 * @property DnsTypes $type
 * @property $value
 * @property int $host_id
 */
class Dns extends Model
{
    private static string $ptrPrefix = '.in-addr.arpa';

    protected $table = 'dns';

    protected $fillable = ['host_id', 'type', 'value'];

    protected $casts = [
        'type' => DnsTypes::class,
        'value' => Json::class,
    ];

    protected static function boot()
    {
        static::created(function (Dns $dns) {
            if ($dns->canBeCreatedToPTR()) {
                try {
                    $this->createPtrRecord($dns);
                } catch (\Exception $exception) {
                    Log::error($exception->getMessage());
                }
            }
        });

        parent::boot();
    }

    private function canBeCreatedToPTR(): bool
    {
        return $this->type === DnsTypes::A;
    }

    private function createPtrRecord(self $dns): void
    {
        $record = dns_get_record($dns->value['ip'] . self::$ptrPrefix, DNS_PTR);

        try {
            Dns::query()->create([
                'host_id' => $dns->host_id,
                'type' => DnsTypes::PTR->value,
                'value' => $record,
            ]);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }
    }

    public function host(): BelongsTo
    {
        return $this->belongsTo(Host::class);
    }
}
