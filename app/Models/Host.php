<?php

namespace App\Models;

use App\Jobs\SyncDnsRecordsFromHost;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string $name
 */
class Host extends Model
{
    protected $fillable = ['name'];

    protected static function boot()
    {
        static::created(function (Host $host) {
            SyncDnsRecordsFromHost::dispatchSync($host);
        });

        static::updated(function (Host $host) {
            SyncDnsRecordsFromHost::dispatchSync($host);
        });

        parent::boot();
    }

    public function dnsRecords(): HasMany
    {
        return $this->hasMany(Dns::class);
    }
}
