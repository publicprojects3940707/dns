<?php

namespace App\Jobs;

use App\Models\Host;
use App\Services\HostService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SyncDnsRecordsFromHost implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private Host $host;

    public function __construct(Host $host)
    {
        $this->host = $host;
    }

    /**
     * Execute the job.
     */
    public function handle(HostService $hostService): void
    {
        try {
            $hostService->syncDnsRecordsFromHost($this->host);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }
    }
}
