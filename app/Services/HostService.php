<?php

namespace App\Services;

use App\Models\Host;

class HostService
{
    public function updateOrCreate(array $hostNames): void
    {
        foreach ($hostNames as $hostName) {
            $host = Host::where('name', $hostName['name'])->first();

            $host ? $this->update($host) : $this->create($hostName);
        }
    }

    /**
     * @param Host $host
     * @return void
     */
    public function syncDnsRecordsFromHost(Host $host): void
    {
        /** @var DnsService $dnsService */
        $dnsService = app(DnsService::class);
        $dnsRecords = dns_get_record($host->name);

        $dnsService->refreshHostDnsRecords($dnsRecords, $host);
    }

    private function create(array $hostName): void
    {
        Host::create($hostName);
    }

    private function update(Host $host): void
    {
        $host->touch();
    }
}
