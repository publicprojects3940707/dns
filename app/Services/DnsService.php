<?php

namespace App\Services;

use App\Models\Dns;
use App\Models\Host;
use Illuminate\Support\Facades\Log;

class DnsService
{
    public function refreshHostDnsRecords(array $records, Host $host) : void
    {
        $host->dnsRecords()->delete();

        foreach ($records as $record) {
            try {
                Dns::query()->create([
                    'host_id' => $host->getKey(),
                    'type' => $record['type'],
                    'value' => $record,
                ]);
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
            }
        }
    }
}
