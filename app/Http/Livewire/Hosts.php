<?php

namespace App\Http\Livewire;

use App\Events\DnsReceived;
use App\Models\Host;
use App\Services\HostService;
use Illuminate\Support\Collection;
use Illuminate\View\View;
use Livewire\Component;

class Hosts extends Component
{
    public Collection $inputs;

    protected $rules = [
        'inputs.*.host' => 'required|string',
    ];

    protected $messages = [
        'inputs.*.host.required' => 'This `host` field is required.',
    ];

    public function submit(HostService $hostService): void
    {
        $hosts = $this->validate()['inputs'];
        $this->prepareHostsForSaving($hosts);

        $hostService->updateOrCreate($hosts);
    }

    public function addInput(): void
    {
        $this->inputs->push(['host' => '']);
    }

    public function removeInput($key): void
    {
        $this->inputs->pull($key);
    }


    public function mount()
    {
        $this->fill([
            'inputs' => collect([['host' => '']]),
        ]);
    }

    public function render(): View
    {
        DnsReceived::dispatch();

        return view('livewire.hosts', [
            'hosts' => Host::all(),
        ]);
    }

    private function prepareHostsForSaving(array &$hosts)
    {
        foreach ($hosts as &$host) {
            $host['name'] = $host['host'];
            unset($host['host']);
        }
    }

    public function reload(HostService $hostService, Host $host)
    {
        $hostService->syncDnsRecordsFromHost($host);

        return back();
    }
}
